/**
 * @flow
 */
import { Typography, Colors, Assets } from 'react-native-ui-lib';
import { Dimensions, Platform } from 'react-native';

import colors from './colors';
import fonts from './fonts';
import commonStyles from './common';

const { width } = Dimensions.get('window');

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth: number = 350;

Colors.loadColors(colors);

Typography.loadTypographies({
  title: {
    fontSize: 35,
    ...Platform.select({
      ios: {
        fontWeight: 'bold',
      },
    }),
  },
  h1: {
    fontSize: 34,
    ...Platform.select({
      ios: {
        fontWeight: 'bold',
      },
    }),
  },
  rubik18: {
    fontSize: 18,
    lineHeight: 22,
  },
  rubik20Bold: {
    fontSize: 20,
    lineHeight: 22,
    fontWeight: 'bold',
  },
  rubik24Bold: {
    fontSize: 24,
    lineHeight: 22,
    fontWeight: 'bold',
  },
  h1Rubik: {
    fontSize: 28,
    ...Platform.select({
      ios: {
        fontWeight: '500',
      },
    }),
  },
  h2: {
    fontSize: 18,
    lineHeight: 22,
  },
  h3: {
    fontSize: 16,
    lineHeight: 22,
    ...Platform.select({
      ios: {
        fontWeight: 'bold',
      },
    }),
  },
  h4: {
    fontSize: 14,
    lineHeight: 22,
  },
  p: {
    fontSize: 14,
    lineHeight: 20,
    ...Platform.select({
      ios: {
        fontWeight: '400',
      },
    }),
  },
  default: {
    fontSize: 18,
  },
  defaultMedium: {
    fontSize: 18,
  },
  defaultLight: {
  },
});

Assets.loadAssetsGroup('images', {});

Assets.loadAssetsGroup('icons', {});

const scale = (size: number): number => (width / guidelineBaseWidth) * size;

export { colors, fonts, scale, commonStyles };

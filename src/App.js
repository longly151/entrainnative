import React from "react";
import {Root} from "native-base";
// import { StackNavigator, DrawerNavigator } from "react-navigation";
import {createDrawerNavigator, createStackNavigator, createAppContainer} from "react-navigation";


import Profile from "./screens/profile";
import Course from "./screens/course";
import User from "./screens/user";

import Login from "./screens/login";
import SideBar from "./screens/sidebar";

import AddUser from "./screens/user/add";
import ManageUser from "./screens/user/manage";
import InfoUser from "./screens/user/infoUser";

import AllCourse from "./screens/course/allCourse";
import DetailCourse from "./screens/course/detailCourse";
import MyCourse from "./screens/course/myCourse";
import CourseByUser from "./screens/course/courseByUser";
import ClientView from "./screens/course/clientView";

import NavigationService from "./NavigationService";

const Drawer = createDrawerNavigator(
  {
    Profile: {screen: Profile},
    Course: {screen: Course},
    User: {screen: User}
  },
  {
    initialRouteName: "Course",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = createStackNavigator(
  {
    Login: {screen: Login},
    Drawer: {screen: Drawer},

    AddUser: {screen: AddUser},
    InfoUser: {screen: InfoUser},
    ManageUser: {screen: ManageUser},


    AllCourse: {screen: AllCourse},
    MyCourse: {screen: MyCourse},
    CourseByUser: {screen: CourseByUser},
    ClientView: {screen: ClientView},
    DetailCourse: {screen: DetailCourse},
    // Header1: {screen: Header1},
    // Header2: {screen: Header2},
    // Header3: {screen: Header3},
    // Header4: {screen: Header4},
    // Header5: {screen: Header5},
    // Header6: {screen: Header6},
    // Header7: {screen: Header7},
    // Header8: {screen: Header8},
    // HeaderSpan: {screen: HeaderSpan},
    // HeaderNoShadow: {screen: HeaderNoShadow},
    // HeaderTransparent: {screen: HeaderTransparent},

  },
  {
    initialRouteName: "Login",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default () =>
  <Root>
    <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
  </Root>;

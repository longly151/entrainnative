import {createUrl, getHandle, postHandle} from "./index";

async function addUser(params) {
  return postHandle(createUrl("users"),params);
}
async function getUsers(params) {
  return getHandle(`${createUrl("users")}?${params}`);
}
async function getUser(id) {
  return getHandle(`${createUrl("users")}/${id}`);
}
async function banUser(id) {
  return getHandle(`${createUrl("users/ban")}/${id}`);
}
async function activateUser(id) {
  return getHandle(`${createUrl("users/activate")}/${id}`);
}

export {
  addUser,
  getUsers,
  getUser,
  banUser,
  activateUser
};

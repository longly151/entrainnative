import {createUrl, getHandle, postHandle} from "./index";


async function login(params) {
  return postHandle(createUrl("auth/login"),params);
}
async function logout() {
  return getHandle(createUrl("auth/logout"));
}
async function getCurrentUser() {
  return getHandle(createUrl("auth/me"));
}
async function changeInfo(params) {
  return postHandle(createUrl("auth/change-info"),params);
}
async function changePassword(params) {
  return postHandle(createUrl("auth/change-password"),params);
}

export {
  login,
  logout,
  getCurrentUser,
  changeInfo,
  changePassword
};

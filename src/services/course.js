import {createUrl, getHandle, postHandle} from "./index";
import {Linking} from "react-native";
import {getLocalData, issetLocalData} from "../helpers/function";

async function getCourses(params) {
  return getHandle(`${createUrl("courses")}?${params}`);
}
async function getMyCourses(params) {
  return getHandle(`${createUrl("courses/me")}?${params}`);
}
async function getCourseByUser(id,params) {
  return getHandle(`${createUrl("courses/user-courses/")}${id}?${params}`);
}
async function getCourse(id) {
  return getHandle(`${createUrl("courses")}/${id}`);
}
async function registerCourse(params) {
  return postHandle(createUrl("courses/request-join"),params);
}
async function downloadFile(encodedFileName) {
  const access_token = await issetLocalData("local_access_token") ? await getLocalData("local_access_token") : "";
  const uri = createUrl(`courses/download/${encodedFileName}/?token=${access_token}`);
  return Linking.openURL(uri);
}

export {
  getCourses,
  getCourse,
  registerCourse,
  getMyCourses,
  getCourseByUser,
  downloadFile
};

import {getLocalData, clearLocalData, issetLocalData} from "../helpers/function";
import NavigationService from "../NavigationService";
import { NavigationActions, StackActions } from 'react-navigation';
import {logout} from "./auth";
import {Toast} from "native-base";
// const baseURL = "http://entrain.local.com/api";
const baseURL = "https://entrain.cf/api";
// const baseURL = "http://18.222.197.227:3005/api";

function createUrl(slug) {
  return `${baseURL}/${slug}`;
}

// async function getHandle(url) {
//   const access_token = await issetLocalData("local_access_token") ? await getLocalData("local_access_token") : "";
//   try {
//     let response = await fetch(url, {
//       method: "GET",
//       headers: {
//         "Accept": "application/json",
//         "Content-Type": "application/json",
//         "Authorization": `Bearer ${access_token}`
//       },
//     });
    
//     if (response.status === 403) {
//       console.log("ok1");
      
//       // await clearLocalData();
//       // await logout();
//       console.log("ok2");
//       // return NavigationService.navigate("Login", { error: "Nope" });
//     }
//     else {
//       console.log("ok2".response.status);
//       let responseJson = await response.json();
//       return responseJson;
//     }
//   } catch (error) {
//     console.log(`Error is : ${error}`);
//   }
// }

async function getHandle(url) {
  const access_token = await issetLocalData("local_access_token") ? await getLocalData("local_access_token") : "";
  try {
    let response = await fetch(url, {
      method: "GET",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${access_token}`
      },
    });
    if (response.status === 403) {
      const error_message = await response.json();
      await clearLocalData();
      await logout();
      NavigationService.reset("Login",error_message);
    }
    else {
      let responseJson = await response.json();
      return responseJson;
    }
  } catch (error) {
    Toast.show({
      text: `${error.message}`,
      textStyle: {
        textAlign: "center"
      },
      type: "danger"
    });
    return;
  }
}
async function postHandle(url, params) {
  const access_token = await issetLocalData("local_access_token") ? await getLocalData("local_access_token") : "";
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${access_token}`
      },
      body: JSON.stringify(params)
    });
    if (response.status === 403) {
      const error_message = await response.json();
      await clearLocalData();
      await logout();
      NavigationService.reset("Login",error_message);
    }
    else {
      let responseJson = await response.json();
      return responseJson;
    }
  } catch (error) {
    Toast.show({
      text: `${error.message}`,
      textStyle: {
        textAlign: "center"
      },
      type: "danger"
    });
    return;
  }
}

export {
  getHandle,
  postHandle,
  createUrl,
};

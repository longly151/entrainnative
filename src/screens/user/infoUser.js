import React, {Component} from "react";
import {Image, View} from "react-native";
import {
  Container, Header, Title, Content,
  Button, Text, Body, Left,
  Right, Icon, Card, CardItem,
  ListItem
} from "native-base";
import styles from "./styles";

import {renderLoading} from "../../helpers/render";
import {getUser} from "../../services/user";

class InfoUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname: "",
      phone_number: "",
      email: "",
      address: "",
      description: "",
      position: "",
      loading: true,
    };
  }
  async componentWillMount() {
    let currentUserFromServer = await getUser(this.props.navigation.state.params.id);
    let currentUser = currentUserFromServer.data;
    this.setState({
      fullname: currentUser.fullname,
      phone_number: currentUser.phone_number,
      email: currentUser.email,
      avatar: currentUser.avatar,
      address: currentUser.address,
      position: currentUser.position,
      description: currentUser.description,
      loading: false,
    });
  }

  render() {
    renderLoading(this.state.loading);
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>User Info</Title>
          </Body>
          <Right />
        </Header>
        {this.state.loading ? renderLoading(this.state.loading)
          :
          <Content>
            <View style={styles.drawerImage}>
              <Image style={styles.largeAvatar} source={{uri: this.state.avatar}} />
            </View>
            <ListItem icon>
              <Left>
                <Button style={{backgroundColor: "#4CDA64"}}>
                  <Icon active name="person" />
                </Button>
              </Left>
              <Body>
                <Text><Text style={{fontWeight: "bold"}}>{this.state.fullname}</Text></Text>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Button style={{backgroundColor: "#4CDA64"}}>
                  <Icon active name="phone-portrait" />
                </Button>
              </Left>
              <Body>
                <Text style={{fontWeight: "bold"}}>{this.state.phone_number}</Text>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Button style={{backgroundColor: "#4CDA64"}}>
                  <Icon active name="mail" />
                </Button>
              </Left>
              <Body>
                <Text style={{fontWeight: "bold"}}>{this.state.email}</Text>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Button style={{backgroundColor: "#4CDA64"}}>
                  <Icon active name="pin" />
                </Button>
              </Left>
              <Body>
                <Text style={{fontWeight: "bold"}}>{this.state.address}</Text>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Button style={{backgroundColor: "#4CDA64"}}>
                  <Icon active name="send" />
                </Button>
              </Left>
              <Body>
                <Text style={{fontWeight: "bold"}}>{this.state.position}</Text>
              </Body>
            </ListItem>
            {
              this.state.description ?
                <Card style={{flex: 0}}>
                  <CardItem>
                    <Body>
                      <Text>
                        {this.state.description}
                      </Text>
                    </Body>
                  </CardItem>
                </Card> : null
            }
          </Content>
        }
      </Container>
    );
  }
}

export default InfoUser;

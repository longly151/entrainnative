import React, {Component} from "react";
import {
  View, Text, TouchableOpacity
} from "react-native";
import {
  Container, Header, Left, Button,
  Icon, Body, Title, Right
} from "native-base";
import styles from "./styles";
import FontAwesome from "react-native-vector-icons/FontAwesome5";

class User extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>User</Title>
          </Body>
          <Right />
        </Header>
        
        <View>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate({routeName: "AddUser"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <FontAwesome
              name="user-plus"
              style={[styles.itemImage, {color: "#555CC4", fontSize: 30}]}
            />
            <Text style={styles.itemText}>Add User</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate({routeName: "ManageUser"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <FontAwesome
              name="users-cog"
              style={[styles.itemImage, {color: "#555CC4", fontSize: 30}]}
            />
            <Text style={styles.itemText}>Manage User</Text>
          </TouchableOpacity>
        </View>
        {/* <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.props.parentAdminIndex.props.navigation.navigate({routeName: "HistoryCourse"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <Image
              resizeMode="contain"
              source={calendarIcon}
              style={styles.itemImage}
            />
            <Text style={styles.itemText}>History</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.parentAdminIndex.props.navigation.navigate({routeName: "RequestCourse"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <Image
              resizeMode="contain"
              source={chatIcon}
              style={styles.itemImage}
            />
            <Text style={styles.itemText}>Request</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    
      </Container>
    );
  }
}

export default User;

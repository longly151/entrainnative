import React, {Component} from "react";
import {ListView, Dimensions, Platform, TouchableOpacity} from "react-native";
import {
  Container, Header, ActionSheet, Content,
  Button, Icon, List, ListItem,
  Text, Left, Right, Body, Thumbnail,
  Input, Toast, Badge
} from "native-base";
import styles from "./styles";
import {getUsers, banUser, activateUser} from "../../services/user";
import {renderLoading} from "../../helpers/render";
import AwesomeButton from "react-native-really-awesome-button/src/themes/rick";
import FontAwesome from "react-native-vector-icons/FontAwesome5";

const deviceWidth = Dimensions.get("window").width;

var BUTTONS = [
  {text: "Fullname", icon: "contact", iconColor: "#2c8ef4"},
  {text: "Position", icon: "briefcase", iconColor: "#ea943b"},
  {text: "Status", icon: "checkmark-circle-outline", iconColor: "#f42ced"},
  {text: "Email", icon: "mail", iconColor: "#25de5b"},
  {text: "Phone Number", icon: "phone-portrait", iconColor: "#fa213b"},
  {text: "Cancel", icon: "close", iconColor: "#25de5b"}
];
var CANCEL_INDEX = 5;
class ManageUser extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = ({
      userList: [],
      loading: true,
      userMeta: [],
      page: 1,
      searchContent: "",
      clicked: BUTTONS[0],
    });
  }
  componentWillMount() {
    this.fetchData();
  }
  refreshDataFromServer = (searchField, searchContent) => {
    this.setState({loading: true});
    const params = `limit=20&${searchField}=${searchContent}&fields=id,fullname,position,status,avatar&order=created_at:desc`;
    getUsers(params).then((users) => {
      this.setState({
        userList: users.data,
        userMeta: users.meta,
        loading: false,
      });
      if (!users.data.length) {
        Toast.show({
          text: "User not found, please try again",
          position: "top",
          style: {top: 60},
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
    }).catch(() => {
      this.setState({userList: []});
    });
  }
  fetchData = () => {
    const params = `limit=20&page=${this.state.page}&fields=id,fullname,position,status,avatar&order=created_at:desc`;
    getUsers(params).then((users) => {
      this.setState(state => ({
        userList: [...state.userList, ...users.data],
        userMeta: users.meta,
        loading: false,
      }));
    }).catch(() => {
      this.setState({userList: []});
    });
  }
  handleLoadMore = () => {
    const params = `limit=20&page=${this.state.page + 1}&fields=id,fullname,position,status,avatar&order=created_at:desc`;
    getUsers(params).then((users) => {
      this.setState(state => ({
        userList: [...state.userList, ...users.data],
        userMeta: users.meta,
        loading: false,
        page: this.state.page + 1,
      }));
    }).catch(() => {
      this.setState({userList: []});
    });
  };
  search = () => {
    const searchField = this.state.clicked.text !== "Cancel" ? this.state.clicked.text.toLowerCase().split(" ").join("_") : "fullname";
    this.refreshDataFromServer(searchField, this.state.searchContent);
  }
  _banUser(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    let newData = [...this.state.userList];
    banUser(newData[rowId].id).then((result) => {
      newData[rowId].status = result.status;
      if (result.error) {
        Toast.show({
          text: result.error,
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      else {
        Toast.show({
          text: result.messages,
          textStyle: {
            textAlign: "center"
          },
          type: "success"
        });
      }
    }).catch((e) => {
      Toast.show({
        text: "Something wrong, please try again",
        textStyle: {
          textAlign: "center"
        },
        type: "danger"
      });
    });
    newData[rowId].status = "banned";
    this.setState({userList: newData});
  }
  _activateUser(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    let newData = [...this.state.userList];
    activateUser(newData[rowId].id).then((result) => {
      if (result.error) {
        Toast.show({
          text: result.error,
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      else {
        Toast.show({
          text: result.messages,
          textStyle: {
            textAlign: "center"
          },
          type: "success"
        });
      }
    }).catch((e) => {
      Toast.show({
        text: "Something wrong, please try again",
        textStyle: {
          textAlign: "center"
        },
        type: "danger"
      });
    });
    newData[rowId].status = "active";
    this.setState({userList: newData});
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left style={{flex: 2}}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 6}}>
            <Input style={{width: "100%", color: Platform.OS === "android" ? "#FFF" : "#000"}} autoCorrect={false}
              placeholder={`Search ${this.state.clicked.text !== "Cancel" ? this.state.clicked.text : "Fullname"}`} placeholderTextColor={Platform.OS === "android" ? "#FFF" : "#696969"}
              onSubmitEditing={this.search} onChangeText={(text) => this.setState({searchContent: text})} />
          </Body>
          <Right style={{flex: 2}}>
            <Button transparent onPress={this.search}>
              <Icon name="search" />
            </Button>
            <Button
              transparent
              onPress={() =>
                ActionSheet.show(
                  {
                    options: BUTTONS,
                    cancelButtonIndex: CANCEL_INDEX,
                    title: "Search by..."
                  },
                  buttonIndex => {
                    this.setState({clicked: BUTTONS[buttonIndex]});
                  }
                )}
            >
              <Icon name="options" />
            </Button>
          </Right>
        </Header>
        {this.state.loading ?
          <Content>
            {renderLoading(this.state.loading)}
          </Content>
          :
          this.state.userList.length === 0 ?
            <Content />
            :
            <Content>
              <List
                dataSource={this.ds.cloneWithRows(this.state.userList)}
                renderRow={data =>
                  <ListItem avatar style={{paddingLeft: 20}}>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("InfoUser", {id: data.id})}>
                      <Left>
                        <Thumbnail small source={{uri: data.avatar}} />
                      </Left>
                    </TouchableOpacity>
                    <Body>
                      <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("InfoUser", {id: data.id})}>
                        <Text>
                          {data.fullname}
                        </Text>
                        <Text numberOfLines={1} note>
                          {data.position}
                        </Text>
                        </TouchableOpacity>
                    </Body>
                    <Right>
                      {
                        data.status === "active" ?
                          <Badge success>
                            <Text>{data.status}</Text>
                          </Badge>
                          :
                          data.status === "pending" ?
                            <Badge warning>
                              <Text>{data.status}</Text>
                            </Badge>
                            :
                            <Badge danger>
                              <Text>{data.status}</Text>
                            </Badge>
                      }

                    </Right>
                  </ListItem>}
                renderLeftHiddenRow={data =>
                  <Button
                    full
                    info
                    onPress={() => this.props.navigation.navigate("CourseByUser", {user_id: data.id})}
                    style={{
                      flex: 1,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >

                    <Icon active name="book" style={{color: "#FFF", fontSize: 28}} />
                  </Button>}
                renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                  data.status === "active" ?
                    <Button
                      full
                      danger
                      onPress={_ => this._banUser(secId, rowId, rowMap)}
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <FontAwesome name="user-slash" style={{color: "#FFF", fontSize: 22}} />
                    </Button>
                    :
                    <Button
                      full
                      success
                      onPress={_ => this._activateUser(secId, rowId, rowMap)}
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <FontAwesome name="user-check" style={{color: "#FFF", fontSize: 22}} />
                    </Button>
                }
                leftOpenValue={75}
                rightOpenValue={-75}
              />
              {
                (this.state.userMeta.current_page !== this.state.userMeta.last_page) ?
                  <AwesomeButton
                    primary
                    width={deviceWidth / 2}
                    style={{margin: 15, marginTop: 20, alignSelf: "center"}}
                    onPress={this.handleLoadMore}
                    type="primary">Load More...
                </AwesomeButton>
                  :
                  <Button />
              }
            </Content>
        }
      </Container>
    );
  }
}

export default ManageUser;

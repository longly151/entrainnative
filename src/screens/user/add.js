import React, {Component} from "react";
import {Keyboard, Platform} from "react-native";
import {
  Container, Header, Title, Content,
  Button, Text, Body, Left,
  Right, Icon, Form, Item,
  Input, Toast, Picker
} from "native-base";
import styles from "./styles";

import {renderLoading} from "../../helpers/render";
import {addUser} from "../../services/user";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname: "",
      username: "",
      phone_number: "",
      email: "",
      address: "",
      position: "Engineer",
      password: "", rePassword: "",
      loading: false,
      shouldUpdate: false,
      errorMessage: [],
    };
    this._onPressAddUser = this._onPressAddUser.bind(this);
  }
  onValueChange2(value: string) {
    this.setState({
      position: value
    });
  }
  _onPressAddUser = () => {
    this.setState({loading: true});
    Keyboard.dismiss();
    const infoData = {
      fullname: this.state.fullname,
      username: this.state.username,
      email: this.state.email,
      phone_number: this.state.phone_number,
      address: this.state.address,
      position: this.state.position,
      password: this.state.password,
      rePassword: this.state.rePassword,
    };
    addUser(infoData).then((result) => {
      if (result.error) {
        this.setState({
          errorMessage: {
            fullname: result.error.fullname ? result.error.fullname[0] : null,
            username: result.error.username ? result.error.username[0] : null,
            email: result.error.email ? result.error.email[0] : null,
            phone_number: result.error.phone_number ? result.error.phone_number[0] : null,
            address: result.error.address ? result.error.address[0] : null,
            position: result.error.position ? result.error.position[0] : null,
            password: result.error.password ? result.error.password[0] : null,
            rePassword: result.error.rePassword ? result.error.rePassword[0] : null,
          }
        });
      }
      if (result.success) {
        this.setState({errorMessage: []});
        Toast.show({
          text: result.success,
          textStyle: {
            textAlign: "center"
          },
          type: "success"
        });
      }
      this.setState({loading: false});
    });
  }

  render() {
    renderLoading(this.state.loading);
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Add User</Title>
          </Body>
          <Right />
        </Header>

        <Content>
        <Form>
            <Item>
              <Icon style={styles.iconSize} active name="person" />
              <Input onChangeText={(text) => this.setState({fullname: text})}
                placeholder="Fullname *" defaultValue={this.state.fullname}
                autoCapitalize="words" autoCorrect={false}
                onSubmitEditing={() => this.refs.username._root.focus()}/>
            </Item>
            {this.state.errorMessage.fullname ? <Text style={styles.errorMessage}>{this.state.errorMessage.fullname}</Text> : null}
            <Item>
              <Icon style={styles.iconSize} active name="card" />
              <Input onChangeText={(text) => this.setState({username: text})}
                placeholder="Username *" defaultValue={this.state.username}
                autoCapitalize="none" autoCorrect={false} ref={"username"}
                onSubmitEditing={() => this.refs.email._root.focus()} />
            </Item>
            {this.state.errorMessage.username ? <Text style={styles.errorMessage}>{this.state.errorMessage.username}</Text> : null}
            <Item>
              <Icon style={styles.iconSize} active name="mail" />
              <Input onChangeText={(text) => this.setState({email: text})}
                placeholder="Email *" keyboardType="email-address" defaultValue={this.state.email}
                autoCapitalize="none" autoCorrect={false} ref={"email"}
                onSubmitEditing={() => this.refs.address._root.focus()}
                />
            </Item>
            {this.state.errorMessage.email ? <Text style={styles.errorMessage}>{this.state.errorMessage.email}</Text> : null}
            <Item>
              <Icon style={styles.iconSize} active name="pin" />
              <Input onChangeText={(text) => this.setState({address: text})}
                placeholder="Address" defaultValue={this.state.address}
                autoCorrect={false} autoCapitalize="words" ref={"address"}
                onSubmitEditing={() => this.refs.phone_number._root.focus()}/>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="call" />
              <Input onChangeText={(text) => this.setState({phone_number: text})}
                placeholder="Phone Number *" keyboardType="phone-pad" defaultValue={this.state.phone_number}
                autoCorrect={false} ref={"phone_number"} />
            </Item>
            {this.state.errorMessage.phone_number ? <Text style={styles.errorMessage}>{this.state.errorMessage.phone_number}</Text> : null}
            <Item>
              <Icon style={styles.iconSize} name="send" />
              <Item picker>
                <Picker
                  mode="dropdown"
                  style={{width: undefined, marginLeft: Platform.OS === "ios" ? -11.5 : -3.5}}
                  placeholder="Select your Position *"
                  placeholderStyle={{color: "#575757",fontSize:17}}
                  placeholderIconColor="#007aff"
                  selectedValue={this.state.position}
                  onValueChange={this.onValueChange2.bind(this)}
                >
                  <Item label="Engineer" value="Engineer" />
                  <Item label="Cadet" value="Cadet" />
                  <Item label="Intern" value="Intern" />
                </Picker>
              </Item>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="lock" />
              <Input onChangeText={(text) => this.setState({password: text})}
                placeholder="Password *" secureTextEntry defaultValue={this.state.password}
                autoCapitalize="none" autoCorrect={false}
                onSubmitEditing={() => this.refs.rePassword._root.focus()}/>
            </Item>
            {this.state.errorMessage.password ? <Text style={styles.errorMessage}>{this.state.errorMessage.password}</Text> : null}
            <Item>
              <Icon style={styles.iconSize} active name="lock" />
              <Input onChangeText={(text) => this.setState({rePassword: text})}
                placeholder="Retype Password *" secureTextEntry defaultValue={this.state.rePassword}
                autoCapitalize="none" autoCorrect={false} ref={"rePassword"}/>
            </Item>
            {this.state.errorMessage.rePassword ? <Text style={styles.errorMessage}>{this.state.errorMessage.rePassword}</Text> : null}
            <Button block style={{margin: 15, marginTop: 20}}
              onPress={this._onPressAddUser}>
              <Text>Add User</Text>
            </Button>
          </Form>
          {renderLoading(this.state.loading)}
        </Content>
      </Container>
    );
  }
}

export default AddUser;

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    backgroundColor: "#fff"
  },
  drawerImage: {
    flex:1,
    justifyContent: "center",
    alignItems: "center",
    width: deviceWidth,
    marginTop:25,
    marginBottom:25,
  },
  avatar: {
    flex:1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 150,
    width: 150,
    borderRadius: 150 / 2
  },
  iconSize: {
    width:30,
  }
};

import React, {Component} from "react";
import {Image, View, Keyboard} from "react-native";
import {
  Container, Header, Title, Content,
  Button, Footer, FooterTab, Text,
  Body, Left, Right, Icon, Card,
  CardItem, ListItem, Form, Item,
  Input, Toast
} from "native-base";
import styles from "./styles";

import {getLocalData} from "../../helpers/function";
import {renderLoading} from "../../helpers/render";
import {changeInfo, changePassword} from "../../services/auth";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab1: true,
      tab2: false,
      tab3: false,
      fullname: "",
      phone_number: "", changedPhone_number: "",
      email: "", changedEmail: "",
      address: "", changedAddress: "",
      description: "", changedDescription: "",
      position: "",
      loading: true,
      shouldUpdate: false,
    };
    this._onPressChangeInfo = this._onPressChangeInfo.bind(this);
  }
  async componentWillMount() {
    let currentUser = JSON.parse(await getLocalData("current_user"));
    this.setState({
      fullname: currentUser.fullname,
      phone_number: currentUser.phone_number, changedPhone_number: currentUser.phone_number,
      email: currentUser.email, changedEmail: currentUser.email,
      avatar: currentUser.avatar,
      address: currentUser.address, changedAddress: currentUser.address,
      position: currentUser.position,
      description: currentUser.description, changedDescription: currentUser.description,
      oldPassword: "", password: "", rePassword: "",
      loading: false,
    });
  }
  toggleTab1() {
    this.setState({
      tab1: true,
      tab2: false,
      tab3: false,
    });
  }
  toggleTab2() {
    this.setState({
      tab1: false,
      tab2: true,
      tab3: false,
    });
  }
  toggleTab3() {
    this.setState({
      tab1: false,
      tab2: false,
      tab3: true,
    });
  }
  _onPressChangeInfo = () => {
    this.setState({loading: true});
    Keyboard.dismiss();
    const infoData = {
      phone_number: this.state.changedPhone_number,
      email: this.state.changedEmail,
      address: this.state.changedAddress,
      description: this.state.changedDescription,
    };
    changeInfo(infoData).then((result) => {
      if (result.error) {
        let errorEmail = "";
        let errorPhone = "";
        if (result.error.email) {
          errorEmail = result.error.email[0];
        }
        if (result.error.phone_number) {
          errorPhone = errorEmail ? `\n${result.error.phone_number[0]}` : `${result.error.phone_number[0]}`;
        }
        Toast.show({
          text: `${errorEmail}${errorPhone}`,
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      else {
        Toast.show({
          text: result.success,
          textStyle: {
            textAlign: "center"
          },
          type: "success"
        });
        this.setState({
          phone_number: this.state.changedPhone_number,
          email: this.state.changedEmail,
          address: this.state.changedAddress,
          description: this.state.changedDescription,
        });
      }
      this.setState({loading: false});
    });
  }
  _onPressChangePassword = () => {
    this.setState({loading: true});
    Keyboard.dismiss();
    const infoPassword = {
      oldPassword: this.state.oldPassword,
      password: this.state.password,
      rePassword: this.state.rePassword
    };
    changePassword(infoPassword).then((result) => {
      if (result.error) {
        let errorOldPassword = "";
        let errorPassword = "";
        let errorRePassword = "";
        if (result.error.oldPassword) {
          errorOldPassword = result.error.oldPassword[0];
        }
        if (result.error.password) {
          errorPassword = errorOldPassword ? `\n${result.error.password[0]}` : `${result.error.password[0]}`;
        }
        if (result.error.rePassword) {
          errorRePassword = (errorOldPassword || errorPassword) ? `\n${result.error.rePassword[0]}` : `${result.error.rePassword[0]}`;
        }
        Toast.show({
          text: `${errorOldPassword}${errorPassword}${errorRePassword}`,
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      else {
        Toast.show({
          text: result.success,
          textStyle: {
            textAlign: "center"
          },
          type: "success"
        });
      }
      this.setState({loading: false});
      // return Keyboard.dismiss();
    });
  }
  getpage() {
    if (this.state.tab1) {
      return (
        <Content>
          <View style={styles.drawerImage}>
            <Image style={styles.avatar} source={{uri: this.state.avatar}} />
          </View>
          <ListItem icon>
            <Left>
              <Button style={{backgroundColor: "#4CDA64"}}>
                <Icon active name="person" />
              </Button>
            </Left>
            <Body>
              <Text><Text style={{fontWeight: "bold"}}>{this.state.fullname}</Text></Text>
            </Body>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{backgroundColor: "#4CDA64"}}>
                <Icon active name="phone-portrait" />
              </Button>
            </Left>
            <Body>
              <Text style={{fontWeight: "bold"}}>{this.state.phone_number}</Text>
            </Body>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{backgroundColor: "#4CDA64"}}>
                <Icon active name="mail" />
              </Button>
            </Left>
            <Body>
              <Text style={{fontWeight: "bold"}}>{this.state.email}</Text>
            </Body>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{backgroundColor: "#4CDA64"}}>
                <Icon active name="pin" />
              </Button>
            </Left>
            <Body>
              <Text style={{fontWeight: "bold"}}>{this.state.address}</Text>
            </Body>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{backgroundColor: "#4CDA64"}}>
                <Icon active name="send" />
              </Button>
            </Left>
            <Body>
              <Text style={{fontWeight: "bold"}}>{this.state.position}</Text>
            </Body>
          </ListItem>
          {
            this.state.description ?
              <Card style={{flex: 0}}>
                <CardItem>
                  <Body>
                    <Text>
                      {this.state.description}
                    </Text>
                  </Body>
                </CardItem>
              </Card> : null
          }
        </Content>
      );
    } else if (this.state.tab2) {
      return (
        <View>
          <Form>
            <Item>
              <Icon style={styles.iconSize} active name="mail" />
              <Input onChangeText={(text) => this.setState({changedEmail: text})}
                placeholder="Email *" defaultValue={this.state.changedEmail} autoCapitalize="none" autoCorrect={false}/>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="call" />
              <Input onChangeText={(text) => this.setState({changedPhone_number: text})}
                placeholder="Phone Number *" keyboardType="phone-pad" defaultValue={this.state.changedPhone_number} autoCorrect={false}/>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="pin" />
              <Input onChangeText={(text) => this.setState({changedAddress: text})} autoCapitalize="words"
                placeholder="Address" keyboardType="email-address" defaultValue={this.state.changedAddress} autoCorrect={false}/>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="create" />
              <Input onChangeText={(text) => this.setState({changedDescription: text})} style={{marginTop: 10, marginBottom: 10, padding: 10}}
                multiline={true} defaultValue={this.state.changedDescription} placeholder="Description" />
            </Item>
            <Button block style={{margin: 15, marginTop: 20}}
              onPress={this._onPressChangeInfo}>
              <Text>Update Info</Text>
            </Button>
          </Form>

          {renderLoading(this.state.loading)}
        </View>
      );
    } else {
      return (
        <View>
          <Form>
            <Item>
              <Icon style={styles.iconSize} active name="lock" />
              <Input onChangeText={(text) => this.setState({oldPassword: text})}
                placeholder="Old Password" secureTextEntry defaultValue={this.state.oldPassword}
                autoCapitalize="none" autoCorrect={false}
                onSubmitEditing={() => this.refs.password._root.focus()}/>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="lock" />
              <Input onChangeText={(text) => this.setState({password: text})}
                placeholder="New Password" secureTextEntry defaultValue={this.state.password}
                autoCapitalize="none" autoCorrect={false} ref="password"
                onSubmitEditing={() => this.refs.rePassword._root.focus()}/>
            </Item>
            <Item>
              <Icon style={styles.iconSize} active name="lock" />
              <Input onChangeText={(text) => this.setState({rePassword: text})}
                placeholder="Retype Password" secureTextEntry defaultValue={this.state.rePassword}
                autoCapitalize="none" autoCorrect={false} ref="rePassword"/>
            </Item>
            <Button block style={{margin: 15, marginTop: 20}}
              onPress={this._onPressChangePassword}>
              <Text>Update Password</Text>
            </Button>
          </Form>

          {renderLoading(this.state.loading)}
        </View>

      );
    }
  }
  render() {
    renderLoading(this.state.loading);
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="ios-menu" />
            </Button>
          </Left>
          <Body>
            <Title>Profile</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          {this.getpage()}
        </Content>

        <Footer>
          <FooterTab>
            <Button active={this.state.tab1} onPress={() => this.toggleTab1()}>
              <Icon active={this.state.tab1} name="contact" />
              <Text>Profile</Text>
            </Button>
            <Button active={this.state.tab2} onPress={() => this.toggleTab2()}>
              <Icon active={this.state.tab2} name="information-circle" />
              <Text>Info</Text>
            </Button>
            <Button active={this.state.tab3} onPress={() => this.toggleTab3()}>
              <Icon active={this.state.tab3} name="lock" />
              <Text>Password</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default Profile;

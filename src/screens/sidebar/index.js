import React, {Component} from "react";
import {Image, TouchableOpacity, Linking} from "react-native";
import {
  Content, Text, List, ListItem,
  Icon, Container, Left, Right,
  Badge
} from "native-base";
import styles from "./style";

import {logout} from "../../services/auth";
import {getLocalData, clearLocalData} from "../../helpers/function";
import {renderLoading} from "../../helpers/render";
import {NavigationActions, StackActions} from "react-navigation"

const drawerCover = require("../../../assets/enclave-launchscreen-bg6.jpg");
class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      isLogin: true,
      loading: true,
      fullname: "",
      avatar: "",
      role: "",
      datas: [],
    };
  }

  _logout = async () => {
    await clearLocalData();
    this.setState({loading: true});
    await logout();
    this.setState({isLogin: false});
  }
  _browser = async () => {
    const uri = "https://entrain.cf";
    return Linking.openURL(uri);
  }
  async componentWillMount() {
    let currentUser = JSON.parse(await getLocalData("current_user"));
    let roleName = currentUser.role_id == "1" ? "Admin" : "Member"
    this.setState({
      fullname: currentUser.fullname,
      avatar: currentUser.avatar,
      role: roleName,
      loading: false,
    });
    this.setDatas();
  }
  componentDidUpdate() {
    if (this.state.isLogin === false) {
      const resetAction = StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({routeName: "Login"})],
      });
      this.props.navigation.dispatch(resetAction);
    }
  }
  setDatas = () => {
    if (this.state.role === "Admin") {
      this.setState({
        datas: [
          {
            name: "Course",
            route: "Course",
            icon: "paper",
            bg: "#477EEA"
          },
          {
            name: "User",
            route: "User",
            icon: "contacts",
            bg: "#477EEA"
          },
          // {
          //   name: "Browser",
          //   icon: "globe",
          //   action: "_browser",
          //   bg: "#477EEA"
          // },
          {
            name: "Logout",
            icon: "power",
            action: "_logout",
            bg: "#477EEA"
          },
        ]
      });
    } else {
      this.setState({
        datas: [
          {
            name: "Course",
            route: "Course",
            icon: "paper",
            bg: "#477EEA"
          },
          // {
          //   name: "Browser",
          //   icon: "globe",
          //   action: "_browser",
          //   bg: "#477EEA"
          // },
          {
            name: "Logout",
            icon: "power",
            action: "_logout",
            bg: "#477EEA"
          },
        ]
      });
    }
  }
  render() {
    return this.state.loading ? renderLoading(this.state.loading) : (
      <Container>
        <Content
          bounces={false}
          style={{flex: 1, backgroundColor: "#fff", top: -1}}
        >
          <TouchableOpacity onPress={() => this.props.navigation.navigate({routeName: "Profile"})}>
            <Image source={drawerCover} style={styles.drawerCover} />
            <Text style={styles.drawerText}>{this.state.fullname} </Text>
            <Text style={styles.drawerSmallText}>{this.state.role} </Text>
            <Image style={styles.drawerImage} source={{uri: this.state.avatar}} />
          </TouchableOpacity>
          <List
            dataArray={this.state.datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => data.route ? this.props.navigation.navigate(data.route)
                  :
                  data.action === "_logout" ?
                  this._logout()
                  :
                  this._browser()
              }
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{color: "#777", fontSize: 26, width: 30}}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{flex: 1}}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default SideBar;

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  },
  drawerImage: {
    position: "absolute",
    // left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
    left: 20,
    top: Platform.OS === "android" ? deviceHeight / 17 : deviceHeight / 12,
    height: 75,
    width: 75,
    borderRadius: 75/2,
    resizeMode: "cover"
  },
  drawerText: {
    position: "absolute",
    left: 20,
    top: Platform.OS === "android" ? deviceHeight / 17 + 75 : deviceHeight / 12 + 90,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold'
  },
  drawerSmallText: {
    position: "absolute",
    left: 20,
    top: Platform.OS === "android" ? deviceHeight / 17 + 100 : deviceHeight / 12 + 120,
    height: 75,
    color: '#7CFC00',
    fontSize: 17,
  },
  logoutButton: {
    position: "absolute",
    right: 0,
    height: 50,
    top: deviceHeight / 3.5 - 50,
    fontSize: 17,
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  }
};

import React, {Component} from "react";
import {
  ImageBackground, View, StatusBar, KeyboardAvoidingView,
  TouchableWithoutFeedback, Keyboard,
} from "react-native";
import {NavigationActions, StackActions} from "react-navigation";
import {
  Container, Button, Text, Form,
  Item, Input, Toast,
} from "native-base";

import styles from "./styles";

import {login, getCurrentUser} from "../../services/auth";
import {storeLocalData, issetLocalData} from "../../helpers/function";
import {renderLoading} from "../../helpers/render";

const launchscreenBg = require("../../../assets/enclave-launchscreen-bg0.jpg");
const launchscreenLogo = require("../../../assets/entrain-logo-1022x371.png");

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loading: false,
    };
  }
  async componentWillMount() {
    // this.setState({loading: true});
    let issetToken = await issetLocalData("local_access_token");
    if (issetToken) {
      const resetAction = StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({routeName: "Drawer"})],
      });
      this.props.navigation.dispatch(resetAction);
    }
    if (this.props.navigation.state.params.error) {
      Toast.show({
        text: this.props.navigation.state.params.error,
        textStyle: {
          textAlign: "center"
        },
        type: "danger"
      });
    }
    // this.setState({loading: false});
  }
  // handle login
  _onPressButton = () => {
    if (this.state.username.length === 0 || this.state.password.length === 0) {
      Toast.show({
        text: "Please enter username and password",
        textStyle: {
          textAlign: "center"
        },
        type: "danger"
      });
      return Keyboard.dismiss();
    }
    this.setState({loading: true});
    const loginData = {
      username: this.state.username,
      password: this.state.password
    };
    login(loginData).then((result) => {
      if (result.error) {
        Toast.show({
          text: result.error,
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      else {
        // console.log(result.access_token);
        storeLocalData("local_access_token", result.access_token);
        getCurrentUser().then((cUser) => {
          storeLocalData("current_user", JSON.stringify(cUser));
          this.setState({password: ""});
          const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({routeName: "Drawer"})],
          });
          this.props.navigation.dispatch(resetAction);
        });
      }
      this.setState({loading: false});
    });
    return Keyboard.dismiss();
  }
  // main form
  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Container>
          <StatusBar barStyle="light-content" />
          <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
            <View style={styles.logoContainer}>
              <ImageBackground source={launchscreenLogo} style={styles.logo} />
            </View>
            <KeyboardAvoidingView behavior="padding">
              <Form>
                <Item>
                  <Input placeholder="Username"
                    returnKeyType="next"
                    autoCorrect={false}
                    autoCapitalize="none"
                    defaultValue={this.state.username}
                    onSubmitEditing={() => this.refs.txtPassword._root.focus()}
                    onChangeText={(text) => this.setState({username: text})}
                  />
                </Item>
                <Item last>
                  <Input placeholder="Password"
                    secureTextEntry
                    returnKeyType="go"
                    autoCorrect={false}
                    ref={"txtPassword"}
                    defaultValue={this.state.password}
                    onChangeText={(text) => this.setState({password: text})}
                  />
                </Item>
              </Form>
              <Button block style={{margin: 15, marginTop: 20}}
                onPress={this._onPressButton}>
                <Text>Sign In</Text>
              </Button>
            </KeyboardAvoidingView>
            {renderLoading(this.state.loading)}
          </ImageBackground>
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

export default Login;

const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  imageContainer: {
    // flex: 1,
    width: null,
    height: deviceHeight
  },
  logoContainer: {
    height: 200,
    marginTop: deviceHeight / 8,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? (deviceWidth - 280) / 2 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 280,
    height: 100
  },
  text: {
    color: "#FF5800",
    fontWeight: "bold",
    fontSize: 20,
    bottom: 6,
    marginBottom: 5,
  }
};

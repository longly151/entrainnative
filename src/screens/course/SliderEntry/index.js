import React, {Component} from "react";
import {View, ScrollView, Text, StatusBar, SafeAreaView} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Carousel, {Pagination} from "react-native-snap-carousel";
import {sliderWidth, itemWidth} from "./styles/SliderEntry.style";
import SliderEntry from "./components/SliderEntry";
import styles, {colors} from "./styles/index.style";
// import { this.props.newCourses, ENTRIES2 } from './static/entries';
import {renderLoading} from "../../../helpers/render";

const SLIDER_1_FIRST_ITEM = 1;

export default class Slider extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
        };
        this._renderItemWithParallax = this._renderItemWithParallax.bind(this);
    }

    _renderItemWithParallax({item, index}, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
                parentSliderEntry={this.props.parentSlider}
            />
        );
    }


    mainExample(number, title) {
        const {slider1ActiveSlide} = this.state;

        return (
            <View style={styles.exampleContainer}>
                <Text style={styles.title}>{"Explore new courses"}</Text>
                <Text style={styles.subtitle}>{title}</Text>
                {
                    (Object.keys(this.props.newCourses).length === 0) ?
                        renderLoading(true)
                        :
                        <Carousel
                            ref={c => this._slider1Ref = c}
                            data={Object.keys(this.props.newCourses).length ? this.props.newCourses : []}
                            renderItem={this._renderItemWithParallax}
                            sliderWidth={sliderWidth}
                            itemWidth={itemWidth}
                            hasParallaxImages={true}
                            firstItem={SLIDER_1_FIRST_ITEM}
                            inactiveSlideScale={0.94}
                            inactiveSlideOpacity={0.7}
                            // inactiveSlideShift={20}
                            containerCustomStyle={styles.slider}
                            contentContainerCustomStyle={styles.sliderContentContainer}
                            loop={true}
                            loopClonesPerSide={2}
                            autoplay={true}
                            autoplayDelay={500}
                            autoplayInterval={3000}
                            onSnapToItem={(index) => this.setState({slider1ActiveSlide: index})}
                        />
                }

                <Pagination
                    dotsLength={Object.keys(this.props.newCourses).length ? this.props.newCourses.length : 0}
                    activeDotIndex={slider1ActiveSlide}
                    containerStyle={styles.paginationContainer}
                    dotColor={"rgba(255, 255, 255, 0.92)"}
                    dotStyle={styles.paginationDot}
                    inactiveDotColor={colors.black}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    carouselRef={this._slider1Ref}
                    tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }

    get gradient() {
        return (
            <LinearGradient
                colors={[colors.background1, colors.background2]}
                startPoint={{x: 1, y: 0}}
                endPoint={{x: 0, y: 1}}
                style={styles.gradient}
            />
        );
    }

    render() {
        const courseCarousel = this.mainExample("", "Recommended courses for you");

        return (
            <View style={styles.container}>
                <StatusBar
                    // translucent={true}
                    backgroundColor={"rgba(0, 0, 0, 0.3)"}
                    barStyle={"light-content"}
                />
                {this.gradient}
                <ScrollView
                    style={styles.scrollview}
                    scrollEventThrottle={200}
                    directionalLockEnabled={true}
                >
                    {courseCarousel}
                </ScrollView>
            </View>
        );
    }
}

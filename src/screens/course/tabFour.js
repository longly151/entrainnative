import React, { Component } from "react";
import {
  Content, Text, Body, List,
  ListItem, Thumbnail, Left, Right, Icon
} from "native-base";
import {TouchableOpacity} from "react-native";
import {downloadFile} from "../../services/course";
import FontAwesome from "react-native-vector-icons/FontAwesome5";

export default class TabFour extends Component {
  render() {
    return (
      <Content>
        <List
          dataArray={this.props.course.documents}
          renderRow={data =>
            <ListItem avatar>
              <Left>
                {(()=>{
                  let iconName = "file";
                  const fileExtension = data.filename.split(".")[data.filename.split(".").length - 1 ].toLocaleLowerCase();
                  const imageFormat = ["jpg","gif","png","psd","bmp"];
                  const pdfFormat = ["pdf"];
                  const wordFormat = ["doc","docx"];
                  const excelFormat = ["xls","xlsx"];
                  const powerpointFormat = ["ppt","pptx"];
                  const CompressedFormat = [".7z","rar","zip","zipx"];
                  const textFormat = ["txt","rtf"];
                  if (imageFormat.includes(fileExtension)) {iconName = "file-image";}
                  else if (pdfFormat.includes(fileExtension)) {iconName = "file-pdf";}
                  else if (wordFormat.includes(fileExtension)) {iconName = "file-word";}
                  else if (excelFormat.includes(fileExtension)) {iconName = "file-excel";}
                  else if (powerpointFormat.includes(fileExtension)) {iconName = "file-powerpoint";}
                  else if (CompressedFormat.includes(fileExtension)) {iconName = "file-archive";}
                  else if (textFormat.includes(fileExtension)) {iconName = "file-alt";}
                  return <FontAwesome name={iconName} style={{color:"#00B8FF",fontSize:35}}/>;
                })()}
                {/* <Thumbnail small source={{uri:data.user.avatar}} /> */}
                
              </Left>
              <Body>
                <Text numberOfLines={1}>
                  {data.filename}
                </Text>
                <Text numberOfLines={1} note>
                  {data.user.fullname}
                </Text>
              </Body>
              <Right>
                <TouchableOpacity
                onPress={() => downloadFile(data.encodedFileName)}>
                <Icon name="cloud-download" style={{fontSize:35,color:"#32CD32",alignSelf:"center"}}/>
                </TouchableOpacity>
              </Right>
            </ListItem>}
        />
      </Content>
    );
  }
}

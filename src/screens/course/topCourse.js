import {compose} from "recompose";
import React, {Component} from "react";
import {
  View, Text, FlatList, TouchableOpacity,
  Image
} from "react-native";
import {
  Thumbnail, Badge,
} from "native-base";
import {colors} from "../../styles";

import styles from "./styles";

import {renderLoading} from "../../helpers/render";
import FontAwesome from "react-native-vector-icons/FontAwesome";

class TopCourse extends Component {
  _openArticle = article => {
    this.props.parentTopCourse.props.navigation.navigate("DetailCourse", {
      id: article.id,
      cover_image: article.cover_image,
      title: article.title,
      description: article.description,
      start_time: article.start_time,
      end_time: article.end_time,
      roomName: article.room.name,
    });
  };
  renderRowTwo = ({item, index}) => (
    <TouchableOpacity
      key={item.id}
      style={styles.itemTwoContainer}
      onPress={() => this._openArticle(item)}
    >
      <View style={styles.itemTwoContent}>
        <Image style={styles.itemTwoImage} source={{uri: item.cover_image}} />
        <View style={styles.itemTwoOverlay} />
        <Text style={styles.itemTwoTitle} numberOfLines={1}>{item.title}</Text>
        <View style={[styles.itemThreeMetaContainer, {marginTop: 10}]}>
          <Text style={{color: "#00FF21", fontSize: 15, marginTop: 5, fontWeight: "bold"}}>
            Instructor:
          </Text>
          {
            item.instructors.map(instructor =>
              <TouchableOpacity style={{marginLeft: 5}}
                onPress={() => this.props.parentTopCourse.props.navigation.navigate("InfoUser", {id: instructor.id})}>
                <Thumbnail small source={{uri: instructor.avatar}} />
              </TouchableOpacity>)
          }
        </View>
        <Badge success style={{position: "absolute", bottom: 20, left: 20, height: 35, width: 55, alignItems:"center", justifyContent:"center"}}>
          <Text style={[styles.itemTwoSubTitle, {textAlign: "center"}]}>{item.learner_count} <FontAwesome
            name="graduation-cap"
            style={{fontSize: 15, color: "#fff", lineHeight: 20}}
          /></Text>
        </Badge>

        {index === 0 ?
          <Badge danger style={styles.topCourseBadge}>
            <Text style={[styles.itemTwoSubTitle, {textAlign: "center",fontSize: 20}]}>{index + 1}</Text>
          </Badge>
          :
          index === 1 ?
            <Badge warning style={styles.topCourseBadge}>
              <Text style={[styles.itemTwoSubTitle, {textAlign: "center",fontSize: 20}]}>{index + 1}</Text>
            </Badge>
            :
            <Badge info style={styles.topCourseBadge}>
              <Text style={[styles.itemTwoSubTitle, {textAlign: "center",fontSize: 20}]}>{index + 1}</Text>
            </Badge>
        }

      </View>
    </TouchableOpacity>
  );
  render() {
    return (Object.keys(this.props.topCourses).length === 0) ?
      renderLoading(true)
      :
      (
        <FlatList
          keyExtractor={(item, index) => `${index}`}
          style={{backgroundColor: colors.white}}
          data={this.props.topCourses}
          renderItem={this.renderRowTwo}
        />
      );
  }
}

export default compose(
)(TopCourse);


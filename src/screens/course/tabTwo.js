import React, { Component } from "react";
import {
  Content, Text, Body, List,
  ListItem, Thumbnail, Left
} from "native-base";
import {TouchableOpacity} from "react-native";

export default class TabTwo extends Component {
  render() {
    return (
      <Content>
        <List
          dataArray={this.props.course.instructors}
          renderRow={data =>
            <ListItem avatar>
              <TouchableOpacity
              onPress={() => this.props.parentTab.props.navigation.navigate("InfoUser",{id: data.id})}>
              <Left>
                <Thumbnail small source={{uri:data.avatar}} />
              </Left>
              </TouchableOpacity>

              <Body>
              <TouchableOpacity
              onPress={() => this.props.parentTab.props.navigation.navigate("InfoUser",{id: data.id})}>
                <Text>
                  {data.fullname}
                </Text>
                <Text numberOfLines={1} note>
                  Position: {data.position}
                </Text>
              </TouchableOpacity>
              </Body>
            </ListItem>
          }
        />
      </Content>
    );
  }
}

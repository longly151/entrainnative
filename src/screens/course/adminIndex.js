import React, {Component} from "react";
import {
  View, Text, TouchableOpacity
} from "react-native";

import styles from "./styles";
import FontAwesome from "react-native-vector-icons/FontAwesome5";

class AdminIndex extends Component {
  render() {
    return (
      <View>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.props.parentAdminIndex.props.navigation.navigate({routeName: "AllCourse"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <FontAwesome
              name="th"
              style={[styles.itemImage, {color: "#555CC4", fontSize: 30}]}
            />
            <Text style={styles.itemText}>All Course</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.parentAdminIndex.props.navigation.navigate({routeName: "ClientView"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <FontAwesome
              name="users"
              style={[styles.itemImage, {color: "#555CC4", fontSize: 30}]}
            />
            <Text style={styles.itemText}>Client View</Text>
          </TouchableOpacity>
        </View>
        {/* <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.props.parentAdminIndex.props.navigation.navigate({routeName: "HistoryCourse"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <Image
              resizeMode="contain"
              source={calendarIcon}
              style={styles.itemImage}
            />
            <Text style={styles.itemText}>History</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.parentAdminIndex.props.navigation.navigate({routeName: "RequestCourse"})}
            style={[styles.item, {marginHorizontal: 5}]}
          >
            <Image
              resizeMode="contain"
              source={chatIcon}
              style={styles.itemImage}
            />
            <Text style={styles.itemText}>Request</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}

export default AdminIndex;

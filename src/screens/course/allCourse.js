import React, {Component} from "react";
import {
  View, Platform, Text, FlatList,
  TouchableOpacity, Image, Dimensions,
} from "react-native";
import {
  Container, Header, Content, Button,
  Icon, List, ListItem, Left, Right,
  Body, Thumbnail, Badge, Input,
  Toast
} from "native-base";

import {colors} from "../../styles";
import {GridRow} from "../../components";

import styles from "./styles";

import {renderLoading} from "../../helpers/render";
import {getLocalData} from "../../helpers/function";
import {getCourses} from "../../services/course";
import AwesomeButton from "react-native-really-awesome-button/src/themes/rick";
import moment from "moment";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const deviceWidth = Dimensions.get("window").width;


class FlatListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRowKey: this.props.rowData.item[0].id,
      currentUser: {},
      justRegisteredId: 0,
    };
  }
  justRegistered = (id) => {
    this.setState({justRegisteredId: id});
  }
  _openArticle = article => {
    this.props.parentFlatList.props.navigation.navigate("DetailCourse", {
      id: article.id,
      cover_image: article.cover_image,
      title: article.title,
      description: article.description,
      start_time: article.start_time,
      end_time: article.end_time,
      roomName: article.room.name,
      justRegistered: this.justRegistered.bind(this)
    });
  };
  async componentWillMount() {
    this.setState({currentUser: JSON.parse(await getLocalData("current_user"))})
  }
  render() {
    const cellViews = this.props.rowData.item.map(item => (
      <TouchableOpacity key={item.id} onPress={() => this._openArticle(item)}>
        <View style={[styles.itemOneContainer]}>
          <View style={styles.itemOneImageContainer}>
            <Image style={styles.itemOneImage} resizeMode="cover" source={{uri: item.cover_image}} />
          </View>
          {(() => {
            if (item.id !== this.state.justRegisteredId) {
              let isLearner = false;
              let isInstructor = false;
              item.learners.map(learner => {
                if (learner.id === this.state.currentUser.id) {
                  isLearner = true;
                  return;
                }
              });
              item.instructors.map(instructor => {
                if (instructor.id === this.state.currentUser.id) {
                  isInstructor = true;
                  return;
                }
              });
              return isLearner ?
                <Badge style={styles.badge}>
                  <Text style={{color: "#FFF"}}>Registered</Text>
                </Badge>
                :
                isInstructor ?
                  <Badge warning style={styles.badge}>
                    <Text style={{color: "#FFF"}}>Teaching</Text>
                  </Badge>
                  :
                  (item.max_quantity <= item.learner_count) ?
                    <Badge style={styles.badge}>
                      <Text style={{color: "#FFF"}}>Full Learners</Text>
                    </Badge>
                    :
                    <Badge success style={styles.badge}>
                      <Text style={{color: "#FFF"}}>Available</Text>
                    </Badge>;
            }
            return <Badge style={styles.badge}>
              <Text style={{color: "#FFF"}}>Registered</Text>
            </Badge>;
          })()}

          <View style={styles.itemOneContent}>
            <Text style={[styles.itemOneTitle, {color: "#FFA500", fontWeight: "bold"}]} numberOfLines={2}>
              {item.title}
            </Text>

            <Text
              style={[styles.itemOneTitle, {fontSize: 12, marginTop: 5}]}
              styleName="collapsible"
              numberOfLines={1}
            >
              <Icon
                name="time"
                style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
              /> {moment(item.start_time).format("h:mm  YYYY-MM-DD")}
            </Text>

            <Text
              style={[styles.itemOneTitle, {fontSize: 12, marginTop: 5}]}
              styleName="collapsible"
              numberOfLines={1}
            >
              <FontAwesome
                name="hourglass-half"
                style={{fontSize: 12, lineHeight: 20, color: "#6CC36C"}}
              /> {moment.utc(moment(item.end_time).diff(moment(item.start_time))).format("h:mm")}  <Icon
                name="home"
                style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
              /> {item.room.name}  <Icon
                name="people"
                style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
              /> {item.id === this.state.justRegisteredId ? `${item.learner_count + 1}/${item.max_quantity}` :
                `${item.learner_count}/${item.max_quantity}`}
            </Text>

            <Text style={{color: "#4B69CB", fontSize: 15, marginTop: 5, fontWeight: "bold"}}>
              Instructor
            </Text>
            <List
              dataArray={item.instructors}
              renderRow={data =>
                <ListItem avatar style={{marginLeft: 0}}>
                  <TouchableOpacity
                    onPress={() => this.props.parentFlatList.props.navigation.navigate("InfoUser", {id: data.id})}>
                    <Left>
                      <Thumbnail small source={{uri: data.avatar}} />
                    </Left>
                  </TouchableOpacity>

                  <Body style={{marginTop: 10}}>
                    <TouchableOpacity
                      onPress={() => this.props.parentFlatList.props.navigation.navigate("InfoUser", {id: data.id})}>
                      <Text>
                        {data.fullname}
                      </Text>
                    </TouchableOpacity>
                  </Body>
                </ListItem>

              }
            />
          </View>
        </View>
      </TouchableOpacity>
    ));
    return (
      <View key={this.props.rowData.item[0].id} style={styles.itemOneRow}>
        {cellViews}
      </View>
    );
  }
}

class AllCourse extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      courseList: [],
      loading: true,
      courseMeta: [],
      page: 1,
      searchContent: "",
    });
  }
  componentDidMount() {
    this.fetchData();
  }
  refreshDataFromServer = (searchContent) => {
    this.setState({loading: true});
    const params = `limit=6&s=${searchContent}&fields=id,title,room_id,start_time,end_time,max_quantity,learner_count,cover_image&order=created_at:desc`;
    getCourses(params).then((courses) => {
      if (courses) {
        this.setState({
          courseList: courses.data,
          courseMeta: courses.meta,
          loading: false,
        });
        if (!courses.data.length) {
          Toast.show({
            text: "Course not found, please try again",
            position: "top",
            style: {top: 60},
            textStyle: {
              textAlign: "center"
            },
            type: "danger"
          });
        }
      }
    }).catch(() => {
      this.setState({courseList: []});
    });
  }
  fetchData = () => {
    const params = `limit=6&page=${this.state.page}&fields=id,title,room_id,start_time,end_time,max_quantity,learner_count,cover_image&order=created_at:desc`;
    getCourses(params).then((courses) => {
      if (courses) {
        this.setState(state => ({
          courseList: [...state.courseList, ...courses.data],
          courseMeta: courses.meta,
          loading: false
        }));
      }

    }).catch(() => {
      this.setState({courseList: []});
    });
  }
  handleLoadMore = () => {
    const params = `limit=6&page=${this.state.page + 1}&fields=id,title,room_id,start_time,end_time,max_quantity,learner_count,cover_image&order=created_at:desc`;
    getCourses(params).then((courses) => {
      if (courses) {
        this.setState(state => ({
          courseList: [...state.courseList, ...courses.data],
          courseMeta: courses.meta,
          loading: false,
          page: this.state.page + 1,
        }));
      }
    }).catch(() => {
      this.setState({courseList: []});
    });
  };
  search = () => {
    this.refreshDataFromServer(this.state.searchContent);
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left style={{flex: 2}}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 6}}>
            <Input style={{width: "100%", color: Platform.OS === "android" ? "#FFF" : "#000"}} autoCorrect={false}
              placeholder="Search" placeholderTextColor={Platform.OS === "android" ? "#FFF" : "#696969"}
              onSubmitEditing={this.search} onChangeText={(text) => this.setState({searchContent: text})} />
          </Body>
          <Right style={{flex: 2}}>
            <Button transparent onPress={this.search}>
              <Icon name="search" />
            </Button>
          </Right>
        </Header>
        {this.state.loading ?
          <Content>
            {renderLoading(this.state.loading)}
          </Content>
          :
          <Content>
            <FlatList
              keyExtractor={item => `${item[0].id}`}
              style={{backgroundColor: colors.white, paddingHorizontal: 15}}
              data={GridRow.groupByRows(this.state.courseList, 2)}
              onEndReachedThreshold={5}
              ListFooterComponent={() =>
                (this.state.courseMeta.current_page !== this.state.courseMeta.last_page) ?
                  <AwesomeButton
                    progress
                    width={deviceWidth / 2}
                    style={{margin: 15, marginTop: 20, alignSelf: "center"}}
                    onPress={this.handleLoadMore}
                    type="primary">Load More...
              </AwesomeButton>
                  :
                  <Button />
              }
              renderItem={(rowData, index) => {
                return <FlatListItem rowData={rowData} index={index} parentFlatList={this} />;
              }}
            />
          </Content>
        }
      </Container>
    );
  }
}

export default AllCourse;

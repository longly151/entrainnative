import React, { Component } from "react";
import { Content, Card, CardItem, Text, Body, Badge, Icon} from "native-base";
import styles from "./styles";
import { ScrollView, Dimensions, View } from "react-native";
import HTML from "react-native-render-html";
import Image from "react-native-scalable-image";
import moment from "moment";
import {renderLoading} from "../../helpers/render";
import FontAwesome from "react-native-vector-icons/FontAwesome";

export default class TabOne extends Component {
  render() {
    return (
      <Content padder>
        <Card>
          <CardItem>
            <Body>
              <Image
              width={Dimensions.get("window").width - 50}
              source={{uri: `${this.props.course.cover_image}`}}
              />
            </Body>
          </CardItem>
          <CardItem>
            <Badge primary style={styles.detailedBage}>
              <Text>
              <Icon
                name="time"
                style={{ fontSize: 15, color: "#fff", lineHeight: 20 }}
              /> {moment(this.props.course.start_time).format("h:mm  YYYY-MM-DD")}</Text>
            </Badge>
            <Badge success style={[styles.detailedBage,{marginLeft:5}]}>
              <Text>
              <FontAwesome
                name="hourglass-half"
                style={{ fontSize: 12, color: "#fff", lineHeight: 20 }}
              /> {moment.utc(moment(this.props.course.end_time).diff(moment(this.props.course.start_time))).format("h:mm")}</Text>
            </Badge>
          </CardItem>
          <CardItem>
            <Badge danger style={styles.detailedBage}>
              <Text>
              <Icon
                name="home"
                style={{ fontSize: 15, color: "#fff", lineHeight: 20 }}
              /> {this.props.course.roomName ? this.props.course.roomName : ""}</Text>
            </Badge>
            <Badge warning style={[styles.detailedBage,{marginLeft:5}]}>
              <Text>
              <Icon
                name="people"
                style={{ fontSize: 15, color: "#fff", lineHeight: 20 }}
              /> {this.props.course.learner_count}/{this.props.course.max_quantity}</Text>
            </Badge>
          </CardItem>
          <CardItem>
            <Body>
              <Text style={{fontWeight:"bold",fontSize:22}}>{this.props.course.title}</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
            {this.props.course.description ? <Text style={{fontSize:18}}>{this.props.course.description}</Text>
            :
            renderLoading(!this.props.course.description)}
            </Body>
          </CardItem>
          <CardItem>
            <Body>
    <ScrollView style={{width:"100%"}}>{this.props.course.content ? <HTML html={this.props.course.content} imagesMaxWidth={Dimensions.get("window").width - 50} />:<View/>}</ScrollView>
            </Body>
          </CardItem>
        </Card>
      </Content>
    );
  }
}

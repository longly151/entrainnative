const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import { colors, fonts } from "../../styles";

export default {
  container: {
    backgroundColor: "#FFF",
  },
  imageContainer: {
    width: null,
    height: deviceHeight,
  },
  mb: {
    marginBottom: 15
  },
  row: {
    flexDirection: "row",
    paddingHorizontal: 10,
    marginTop: 10,
  },
  item: {
    flex: 1,
    paddingVertical: 20,
    borderColor: "#829BF8",
    borderWidth: 1,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#FFF",
    opacity: 0.9,
  },
  itemText: {
    color: "#555CC4",
  },
  itemImage: {
    height: 35,
  },

  // All Course
  tabsContainer: {
    alignSelf: "stretch",
    marginTop: 30,
  },
  itemOneContainer: {
    flex: 1,
    width: Dimensions.get("window").width / 2 - 40,
  },
  itemOneImageContainer: {
    borderRadius: 3,
    overflow: "hidden",
  },
  itemOneImage: {
    height: 200,
    width: Dimensions.get("window").width / 2 - 40,
  },
  itemOneTitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  itemOneSubTitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 13,
    color: "#B2B2B2",
    marginVertical: 3,
  },
  itemOnePrice: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  itemOneRow: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10,
  },
  itemOneContent: {
    marginTop: 5,
    marginBottom: 10,
  },
  itemTwoContainer: {
    // paddingBottom: 5,
    backgroundColor: "white",
    // marginVertical: 5,
  },
  itemTwoContent: {
    padding: 20,
    position: "relative",
    borderColor: "#00DCFF",
    borderWidth: 1,
    height: 170,
  },
  itemTwoTitle: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 18,
  },
  itemTwoSubTitle: {
    color: colors.white,
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
    marginVertical: 5,
  },
  itemTwoPrice: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 20,
  },
  itemTwoImage: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  itemTwoOverlay: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: "#6271da",
    opacity: 0.8,
  },
  itemThreeContainer: {
    backgroundColor: "white",
  },
  itemThreeSubContainer: {
    flexDirection: "row",
    paddingVertical: 10,
  },
  itemThreeImage: {
    height: 100,
    width: 100,
  },
  itemThreeContent: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: "space-between",
  },
  itemThreeBrand: {
    fontFamily: fonts.primaryRegular,
    fontSize: 14,
    color: "#617ae1",
  },
  itemThreeTitle: {
    fontFamily: fonts.primaryBold,
    fontSize: 16,
    fontWeight: "bold",
    color: "#FFA500",
  },
  itemThreeSubtitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 12,
    color: "#a4a4a4",
  },
  itemThreeMetaContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  itemThreePrice: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
    color: "#5f5f5f",
    textAlign: "right",
  },
  itemThreeHr: {
    flex: 1,
    height: 1,
    backgroundColor: "#e3e3e3",
    marginRight: -15,
  },
  badge: {
    position: "absolute",
    opacity:0.9,
    top:5,
    right:5,
    justifyContent: "center",
    alignItems: "center"
  },
  myCourseBadge: {
    position: "absolute",
    opacity:0.9,
    top:2,
    left:2,
    justifyContent: "center",
    alignItems: "center"
  },
  detailedBage: {
    justifyContent: "center",
    alignItems: "center"
  },
  topCourseBadge: {
    position: "absolute",
    bottom: 10,
    right: 10,
    height: 45,
    width: 45,
    borderRadius: 22.5,
    justifyContent: "center",
    alignItems: "center"
  },
  searchBar: {
    marginTop: Platform.OS === "ios" ? -6 : 0,
    justifyContent: "center",
    alignItems: "center"
  },
  // For member
  overview: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#FFF",
  },
  margin: {
    marginHorizontal: 25,
  },
  driver: {
    marginBottom: 11,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
};

import React, {Component} from "react";
import {
  Container, Header, Left, Button,
  Icon, Body, Title, Right
} from "native-base";
import styles from "./styles";

import {getLocalData} from "../../helpers/function";
import {getCourses} from "../../services/course";
import moment from "moment";

import AdminIndex from "./adminIndex";
import ClientIndex from "./clientIndex";

class Course extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      newCourses: {},
      recentCourses: {},
      cUserRecentCourses: {},
      topCourses: {},
      loading: false,
    });
  }
  async componentWillMount() {
    let currentUser = JSON.parse(await getLocalData("current_user"));
    this.setState({
      cUser_id: currentUser.id,
    });
    await this.getNewCourses();
    await this.getRecentCourses();
    await this.getTopCourses();
  }
  async getCoursesFromServer(params) {
    this.setState({loading:true});
    let courses = await getCourses(params);
    return courses.data;
  }
  getNewCourses = async () => {
    let data = await this.getCoursesFromServer("limit=5&fields=id,title,description,room_id,start_time,end_time,max_quantity,learner_count,cover_image&order=created_at:desc");
    this.setState({
      newCourses: data
    });
  }
  getRecentCourses = async () => {
    let data = await this.getCoursesFromServer(`limit=5&fields=id,title,room_id,start_time,end_time,max_quantity,learner_count,cover_image&start_time=${moment().format("YYYY-MM-DD")}&order=start_time:asc`);
    this.setState({
      recentCourses: data
    });
  }
  getTopCourses = async () => {
    let data = await this.getCoursesFromServer("limit=3&fields=id,title,room_id,start_time,end_time,max_quantity,learner_count,cover_image&order=learner_count:desc");
    this.setState({
      topCourses: data
    });
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Course</Title>
          </Body>
          <Right />
        </Header>
        {
          this.state.cUser_id === 1 ?
          <AdminIndex parentAdminIndex={this}/>
          :
          <ClientIndex parentClientIndex={this} />
        }
      </Container>
    );
  }
}

export default Course;

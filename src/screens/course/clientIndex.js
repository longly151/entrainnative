import React, {Component} from "react";
import {
  Text, TouchableOpacity, Image,
  SafeAreaView, ScrollView
} from "react-native";
import {Badge, List} from "native-base";
import styles from "./styles";

import {Block, Card} from "../../components/client/componentss";
import Slider from "./SliderEntry/index";
import TopCourse from "./topCourse";
import moment from "moment";
import {renderLoading} from "../../helpers/render";
import FontAwesome from "react-native-vector-icons/FontAwesome5";

class ClientIndex extends Component {
  _openArticle = article => {
    this.props.parentClientIndex.props.navigation.navigate("DetailCourse", {
      id: article.id,
      cover_image: article.cover_image,
      title: article.title,
      description: article.description,
      start_time: article.start_time,
      end_time: article.end_time,
      roomName: article.room.name,
    });
  };
  render() {
    return (
      <SafeAreaView style={styles.overview}>
        <ScrollView contentContainerStyle={{paddingBottom: 25}}>
          <Slider newCourses={this.props.parentClientIndex.state.newCourses}
          parentSlider={this.props.parentClientIndex}/>

          <Block row style={[styles.margin, {marginTop: 18}]}>
            <TouchableOpacity
              onPress={() => this.props.parentClientIndex.props.navigation.navigate({routeName: "AllCourse"})}
              style={[styles.item, {marginRight: 7}]}
            >
              <FontAwesome
                name="th"
                style={[styles.itemImage,{color:"#555CC4",fontSize:30}]}
              />
              <Text style={styles.itemText}>All Course</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.parentClientIndex.props.navigation.navigate({routeName: "MyCourse"})}
              style={styles.item}
            >
              <FontAwesome
                name="user-graduate"
                style={[styles.itemImage,{color:"#555CC4",fontSize:30}]}
              />
              <Text style={styles.itemText}>My Course</Text>
            </TouchableOpacity>
          </Block>

          <Badge style={{width:120,height:35,justifyContent: "center",alignSelf:"center",alignItems: "center",marginTop:25,padding:20,backgroundColor: "#9600FF"}}>
              <Text style={{color: "#FFF",fontSize:18,fontWeight:"bold"}}>Top Course</Text>
            </Badge>
          <Card
            style={{marginTop: 18,marginHorizontal:25,padding:0}}
          >
            <TopCourse topCourses={this.props.parentClientIndex.state.topCourses}
            parentTopCourse={this.props.parentClientIndex}/>
          </Card>


          <Card
            style={[styles.margin, {marginTop: 18}]}
          >
            
            <Text style={{color:"#696969",marginBottom:20,fontSize:18}}>Recent Courses</Text>
            {
              (Object.keys(this.props.parentClientIndex.state.recentCourses).length === 0) ?
              renderLoading(true)
              :
              <List
              dataArray={this.props.parentClientIndex.state.recentCourses}
              renderRow={data =>
              <Block style={styles.driver}>
              <TouchableOpacity activeOpacity={0.8} onPress={() => this._openArticle(data)}>
                <Block row center>
                  <Block>
                    <Image
                      style={styles.avatar}
                      source={{uri: data.cover_image}}
                    />
                  </Block>
                  <Block flex={2}>
                    <Text numberOfLines={2} style={{paddingRight:10}}>{data.title}</Text>
                  </Block>
                  <Block>
                    <Text paragraph right style={{fontSize:13,color:"#A9A9A9"}}>{moment(data.start_time).fromNow()}</Text>
                  </Block>
                </Block>
              </TouchableOpacity>
              </Block>
              }
            />
            }
          </Card>


          {/* <Card
            title="TRIPS BY TYPE"
            style={[styles.margin, {marginTop: 18}]}
          >
            <Block>
              <Text>Chart</Text>
            </Block>
            <Block row space="between" style={{marginTop: 25}}>
              <Block>
                <Text h2 light>1,744</Text>
                <Block row center>
                  <Label blue />
                  <Text paragraph color="gray">Confort</Text>
                </Block>
              </Block>
              <Block>
                <Text h2 light>2,312</Text>
                <Block row center>
                  <Label purple />
                  <Text paragraph color="gray">Premium</Text>
                </Block>
              </Block>
            </Block>
          </Card> */}
        </ScrollView>
        </SafeAreaView>
    );
  }
}

export default ClientIndex;

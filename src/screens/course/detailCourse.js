import React, {Component} from "react";
import {
  Container, Header, Title, Button,
  Icon, Tabs, Tab, Text, Right,
  Left, Body, TabHeading, Footer,
  FooterTab, Toast, ScrollableTab
} from "native-base";
import {Dimensions} from "react-native";

import TabOne from "./tabOne";
import TabTwo from "./tabTwo";
import TabThree from "./tabThree";
import TabFour from "./tabFour";

import {getCourse, registerCourse} from "../../services/course";
import AwesomeButton from "react-native-really-awesome-button/src/themes/blue";
import {getLocalData} from "../../helpers/function";

const deviceWidth = Dimensions.get("window").width;

class ConfigTab extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      course: {},
      cUser_id: "",
    });
  }
  async componentWillMount() {
    let currentUser = JSON.parse(await getLocalData("current_user"));
    this.setState({
      cUser_id: currentUser.id,
    });
  }
  componentDidMount() {
    this.courseUpdate();
  }
  courseUpdate = () => {
    getCourse(this.props.navigation.state.params.id).then((result) => {
      if (result.error) {
        Toast.show({
          text: result.error,
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      this.setState({course: result.data});
    }).catch((e) => {
      Toast.show({
        text: "Something wrong, please try again",
        textStyle: {
          textAlign: "center"
        },
        type: "danger"
      });
    });
  }
  _register = (next) => {
    const courseData = {
      id_course: this.props.navigation.state.params.id
    };
    registerCourse(courseData).then((result) => {
      if (result.error) {
        Toast.show({
          text: result.error,
          style: {bottom: 70},
          textStyle: {
            textAlign: "center"
          },
          type: "danger"
        });
      }
      if (result.success) {
        Toast.show({
          text: result.success,
          style: {bottom: 70},
          textStyle: {
            textAlign: "center",
          },
          type: "success"
        });
        this.courseUpdate();
        if (this.props.navigation.state.params.justRegistered) this.props.navigation.state.params.justRegistered(courseData.id_course);
      }
    }).catch((e) => {
      Toast.show({
        text: "Something wrong, please try again",
        style: {bottom: 70},
        textStyle: {
          textAlign: "center"
        },
        type: "danger"
      });
    });
    next();
  }
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button transparent onPress={() => {
              this.props.navigation.goBack();
            }}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Course Detail</Title>
          </Body>
          <Right />
        </Header>

        {(() => {
          if (this.state.cUser_id === 1) {
            return (
              <Tabs renderTabBar={() => <ScrollableTab />}>
                <Tab
                  heading={
                    <TabHeading>
                      <Icon name="alert" />
                      <Text>Info</Text>
                    </TabHeading>
                  }
                >
                  <TabOne course=
                    {{
                      content: this.state.course.content,
                      cover_image: this.props.navigation.state.params.cover_image,
                      title: this.props.navigation.state.params.title,
                      description: this.state.course.description,
                      start_time: this.props.navigation.state.params.start_time,
                      end_time: this.props.navigation.state.params.end_time,
                      roomName: this.props.navigation.state.params.roomName,
                      learner_count: this.state.course.learner_count,
                      max_quantity: this.state.course.max_quantity,
                    }} parentTab={this} />
                </Tab>
                <Tab
                  heading={
                    <TabHeading>
                      <Icon name="person" />
                      <Text>Instructors</Text>
                    </TabHeading>
                  }
                >
                  <TabTwo course={this.state.course} parentTab={this} />
                </Tab>
                <Tab
                  heading={
                    <TabHeading>
                      <Icon name="people" />
                      <Text>Learners</Text>
                    </TabHeading>
                  }
                >
                  <TabThree course={this.state.course} parentTab={this} />
                </Tab>
                <Tab
                  heading={
                    <TabHeading>
                      <Icon name="document" />
                      <Text>Documents</Text>
                    </TabHeading>
                  }
                >
                  <TabFour course={this.state.course} parentTab={this} />
                </Tab>
              </Tabs>
            );
          }
          else if (this.state.course.instructors) {
            let check = false;
            this.state.course.instructors.map(instructor => {
              if (this.state.cUser_id) {
                if (instructor.id == this.state.cUser_id) {
                  check = true;
                  return;
                }
              }
            });
            if (check) {
              return (
                <Tabs renderTabBar={() => <ScrollableTab />}>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="alert" />
                        <Text>Info</Text>
                      </TabHeading>
                    }
                  >
                    <TabOne course=
                      {{
                        content: this.state.course.content,
                        cover_image: this.props.navigation.state.params.cover_image,
                        title: this.props.navigation.state.params.title,
                        description: this.state.course.description,
                        start_time: this.props.navigation.state.params.start_time,
                        end_time: this.props.navigation.state.params.end_time,
                        roomName: this.props.navigation.state.params.roomName,
                        learner_count: this.state.course.learner_count,
                        max_quantity: this.state.course.max_quantity,
                      }} parentTab={this} />
                  </Tab>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="person" />
                        <Text>Instructors</Text>
                      </TabHeading>
                    }
                  >
                    <TabTwo course={this.state.course} parentTab={this} />
                  </Tab>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="people" />
                        <Text>Learners</Text>
                      </TabHeading>
                    }
                  >
                    <TabThree course={this.state.course} parentTab={this} />
                  </Tab>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="document" />
                        <Text>Documents</Text>
                      </TabHeading>
                    }
                  >
                    <TabFour course={this.state.course} parentTab={this} />
                  </Tab>
                </Tabs>
              );
            } else {
              return (
                <Tabs renderTabBar={() => <ScrollableTab />}>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="alert" />
                        <Text>Info</Text>
                      </TabHeading>
                    }
                  >
                    <TabOne course=
                      {{
                        content: this.state.course.content,
                        cover_image: this.props.navigation.state.params.cover_image,
                        title: this.props.navigation.state.params.title,
                        description: this.state.course.description,
                        start_time: this.props.navigation.state.params.start_time,
                        end_time: this.props.navigation.state.params.end_time,
                        roomName: this.props.navigation.state.params.roomName,
                        learner_count: this.state.course.learner_count,
                        max_quantity: this.state.course.max_quantity,
                      }} parentTab={this} />
                  </Tab>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="person" />
                        <Text>Instructors</Text>
                      </TabHeading>
                    }
                  >
                    <TabTwo course={this.state.course} parentTab={this} />
                  </Tab>
                  <Tab
                    heading={
                      <TabHeading>
                        <Icon name="document" />
                        <Text>Documents</Text>
                      </TabHeading>
                    }
                  >
                    <TabFour course={this.state.course} parentTab={this} />
                  </Tab>
                </Tabs>
              );
            }
          }
        })()}
        <Footer>
          <FooterTab>
            {(() => {
              let registered = "false";
              if (this.state.course.learners && this.state.cUser_id) {
                this.state.course.learners.map(learner => {
                  if (this.state.cUser_id == learner.id) {
                    registered = "learner";
                    return;
                  }
                });
                this.state.course.instructors.map(instructor => {
                  if (this.state.cUser_id == instructor.id) {
                    registered = "instructor";
                    return;
                  }
                });
                return this.state.cUser_id == 1 ?
                  <AwesomeButton
                    disabled
                    full
                    width={deviceWidth}
                    onPress={this._register}
                    type="primary">Administrator can't join the course
                </AwesomeButton>
                  :
                  registered === "learner" ?
                    <AwesomeButton
                      disabled
                      full
                      width={deviceWidth}
                      onPress={this._register}
                      type="primary">You have joined this course
                </AwesomeButton>
                    :
                    registered === "instructor" ?
                      <AwesomeButton
                        disabled
                        full
                        width={deviceWidth}
                        onPress={this._register}
                        type="primary">You are the instructor of this course
                </AwesomeButton>
                      :
                      <AwesomeButton
                        progress
                        full
                        width={deviceWidth}
                        onPress={this._register}
                        type="primary">Register
                </AwesomeButton>;
              }
            })()}

          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default ConfigTab;

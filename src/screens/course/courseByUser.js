import {compose, withState} from "recompose";
import React, {Component} from "react";
import {
  View, Platform, Text, FlatList,
  TouchableOpacity, Image, Dimensions
} from "react-native";
import {
  Container, Header, Content, Button,
  Icon, Left, Right, Body,
  Thumbnail, Badge, Input, Toast
} from "native-base";
import {colors} from "../../styles";
import {RadioGroup} from "../../components";

import styles from "./styles";

import {renderLoading} from "../../helpers/render";
import {getCourseByUser} from "../../services/course";
import AwesomeButton from "react-native-really-awesome-button/src/themes/rick";
import moment from "moment";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const deviceWidth = Dimensions.get("window").width;

class CourseByUser extends Component {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.state = ({
      courseList: [],
      loading: true,
      courseMeta: [],
      page: 1,
      searchContent: "",
    });    
  }
  componentDidMount() {
    this.fetchData();
  }
  refreshDataFromServer = (searchContent) => {
    this.setState({loading: true});
    const params = `limit=15&s=${searchContent}&fields=id,title,cover_image,start_time`;
    getCourseByUser(this.props.navigation.state.params.user_id,params).then((courses) => {
      if (courses) {
        this.setState({
          courseList: courses.data,
          courseMeta: courses.meta,
          loading: false,
        });
        if (!courses.data.length) {
          Toast.show({
            text: "Course not found, please try again",
            position: "top",
            style: {top: 110},
            textStyle: {
              textAlign: "center"
            },
            type: "danger"
          });
        }
      }
    }).catch(() => {
      this.setState({courseList: []});
    });
  }
  fetchData = () => {
    const params = "limit=15&page=1&fields=id,title,cover_image,start_time";
    getCourseByUser(this.props.navigation.state.params.user_id,params).then((courses) => {
      if (courses) {
        this.setState(state => ({
          courseList: [...state.courseList, ...courses.data],
          courseMeta: courses.meta,
          loading: false
        }));
      }
    }).catch(() => {
      this.setState({courseList: []});
    });
  }
  _getRenderItemFunction = () =>
    [this.renderRowOne, this.renderRowTwo][
    this.props.tabIndex
    ];
  _openArticle = article => {
    this.props.navigation.navigate("DetailCourse", {
      id: article.id,
      cover_image: article.cover_image,
      title: article.title,
      description: article.description,
      start_time: article.start_time,
      end_time: article.end_time,
      roomName: article.room.name,
    });
  };
  search = () => {
    this.refreshDataFromServer(this.state.searchContent);
  }
  handleLoadMore = (page) => {
    if (page != this.state.courseMeta.last_page) {
      let current_page = page + 1;
      const params = `limit=15&page=${current_page}&fields=id,title,cover_image,start_time`;
      getCourseByUser(this.props.navigation.state.params.user_id,params).then((courses) => {
        if (courses) {
          this.setState(state => ({
            courseList: [...state.courseList, ...courses.data],
            courseMeta: courses.meta,
            loading: false,
            page: current_page,
          }));
        }
      }).catch(() => {
        this.setState({courseList: []});
      });
    }
  };
  renderRowOne = ({item}) => {
    if (item.is_instructor === 0) {
      return (
        <TouchableOpacity
          key={item.id}
          style={styles.itemThreeContainer}
          onPress={() => this._openArticle(item)}
        >
          <View style={styles.itemThreeSubContainer}>
            <Image source={{uri: item.cover_image}} style={styles.itemThreeImage} />
            {
              item.current_status === "Finished" ?
                <Badge style={[styles.myCourseBadge, {backgroundColor: "#696969"}]}>
                  <Text style={{color: "#FFF", fontSize: 12}}>Finished</Text>
                </Badge>
                :
                item.current_status === "Ongoing" ?
                  <Badge info style={styles.myCourseBadge}>
                    <Text style={{color: "#FFF", fontSize: 12}}>Ongoing</Text>
                  </Badge>
                  :
                  <Badge warning style={styles.myCourseBadge}>
                    <Text style={{color: "#FFF", fontSize: 12}}>Upcoming</Text>
                  </Badge>
            }
            <View style={styles.itemThreeContent}>
              <Text style={styles.itemThreeBrand}>
                <Icon
                  name="time"
                  style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
                /> {moment(item.start_time).format("h:mm  YYYY-MM-DD")}  <FontAwesome
                  name="hourglass-half"
                  style={{fontSize: 12, lineHeight: 20, color: "#6CC36C"}}
                /> {moment.utc(moment(item.end_time).diff(moment(item.start_time))).format("h:mm")}  <Icon
                  name="home"
                  style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
                /> {item.room.name}
              </Text>
              <View>
                <Text style={styles.itemThreeTitle} numberOfLines={2}>{item.title}</Text>
              </View>
              <View style={styles.itemThreeMetaContainer}>
                <Text style={{color: "#4B69CB", fontSize: 15, marginTop: 5, fontWeight: "bold"}}>
                  Instructor:
                </Text>
                {
                  item.instructors.map(instructor =>
                    <TouchableOpacity style={{marginLeft: 5}}
                      onPress={() => this.props.navigation.navigate("InfoUser", {id: instructor.id})}>
                      <Thumbnail small source={{uri: instructor.avatar}} />
                    </TouchableOpacity>)
                }
              </View>
            </View>
          </View>
          <View style={styles.itemThreeHr} />
        </TouchableOpacity>
      );
    }
  }
  renderRowTwo = ({item}) => {
    if (item.is_instructor === 1) {
      return (
        <TouchableOpacity
          key={item.id}
          style={styles.itemThreeContainer}
          onPress={() => this._openArticle(item)}
        >
          <View style={styles.itemThreeSubContainer}>
            <Image source={{uri: item.cover_image}} style={styles.itemThreeImage} />
            {
              item.current_status === "Finished" ?
                <Badge style={[styles.myCourseBadge, {backgroundColor: "#696969"}]}>
                  <Text style={{color: "#FFF", fontSize: 12}}>Finished</Text>
                </Badge>
                :
                item.current_status === "Ongoing" ?
                  <Badge info style={styles.myCourseBadge}>
                    <Text style={{color: "#FFF", fontSize: 12}}>Ongoing</Text>
                  </Badge>
                  :
                  <Badge warning style={styles.myCourseBadge}>
                    <Text style={{color: "#FFF", fontSize: 12}}>Upcoming</Text>
                  </Badge>
            }
            <View style={styles.itemThreeContent}>
              <Text style={styles.itemThreeBrand}>
                <Icon
                  name="time"
                  style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
                /> {moment(item.start_time).format("h:mm  YYYY-MM-DD")}  <FontAwesome
                  name="hourglass-half"
                  style={{fontSize: 12, lineHeight: 20, color: "#6CC36C"}}
                /> {moment.utc(moment(item.end_time).diff(moment(item.start_time))).format("h:mm")}  <Icon
                  name="home"
                  style={{fontSize: 14, lineHeight: 20, color: "#6CC36C"}}
                /> {item.room.name}
              </Text>
              <View>
                <Text style={styles.itemThreeTitle} numberOfLines={2}>{item.title}</Text>
              </View>
              <View style={styles.itemThreeMetaContainer}>
                <Text style={{color: "#4B69CB", fontSize: 15, marginTop: 5, fontWeight: "bold"}}>
                  Instructor:
                </Text>
                {
                  item.instructors.map(instructor =>
                    <TouchableOpacity style={{marginLeft: 5}}
                      onPress={() => this.props.navigation.navigate("InfoUser", {id: instructor.id})}>
                      <Thumbnail small source={{uri: instructor.avatar}} />
                    </TouchableOpacity>)
                }
              </View>
            </View>
          </View>
          <View style={styles.itemThreeHr} />
        </TouchableOpacity>
      );
    }
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left style={{flex: 2}}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 6}}>
            <Input style={{width: "100%", color: Platform.OS === "android" ? "#FFF" : "#000"}} autoCorrect={false}
              placeholder="Search" placeholderTextColor={Platform.OS === "android" ? "#FFF" : "#696969"}
              onSubmitEditing={this.search} onChangeText={(text) => this.setState({searchContent: text})} />
          </Body>
          <Right style={{flex: 2}}>
            <Button transparent onPress={this.search}>
              <Icon name="search" />
            </Button>
          </Right>
        </Header>
        {this.state.loading ?
          <Content>
            {renderLoading(this.state.loading)}
          </Content>
          :
          <Content>
            <View style={{height: 50}}>
              <RadioGroup
                selectedIndex={this.props.tabIndex}
                items={this.props.tabs}
                onChange={this.props.setTabIndex}
                underline
              />
            </View>
            <FlatList
              keyExtractor={item => `${item.title}`}
              style={{backgroundColor: colors.white, paddingHorizontal: 15}}
              data={this.state.courseList}
              renderItem={this._getRenderItemFunction()}
              ListFooterComponent={() =>
                (this.state.courseMeta.current_page !== this.state.courseMeta.last_page) ?
                  <AwesomeButton
                    progress
                    width={deviceWidth / 2}
                    style={{margin: 15, marginTop: 20, alignSelf: "center"}}
                    onPress={_ => this.handleLoadMore(this.state.page)}
                    type="primary">Load More...
              </AwesomeButton>
                  :
                  <Button />
              }
            />
          </Content>
        }
      </Container>
    );
  }
}

export default compose(
  withState("tabIndex", "setTabIndex", 0),
  withState("tabs", "setTabs", ["As a learner", "As an instructor"]),
)(CourseByUser);


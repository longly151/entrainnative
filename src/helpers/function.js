import AsyncStorage from '@react-native-community/async-storage';

async function getLocalData(key) {
  try {
    const value = await AsyncStorage.getItem(key)
    if (value !== null) {
      return value;
    } else return;
  } catch (e) {
    console.log(e);
  }
}
async function storeLocalData(key, value) {
  try {
    await AsyncStorage.setItem(key,value);
  } catch (e) {
    console.log(e);
  }
}
async function removeLocalData(key) {
  try {
    await AsyncStorage.removeItem(key)
  } catch(e) {
    console.log(e);
  }
}
async function issetLocalData(key) {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value === undefined || value === null) {
      return false;
    }
    return true;
  } catch (e) {
    console.log(e);
  }
}
async function clearLocalData() {
  try {
    await AsyncStorage.clear();
    return;
  } catch (e) {
    console.log(e);
  }
}


export {
  getLocalData,
  storeLocalData,
  removeLocalData,
  issetLocalData,
  clearLocalData
}
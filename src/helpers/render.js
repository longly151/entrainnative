import React from "react";
import {View} from "react-native";
import {Spinner} from "native-base";


function renderLoading(status) {
  if (status===true) {
    return (
      <View style={{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
      }}>
        <Spinner color="red" />
      </View>
    );
  } else return <View></View>
}

export {
  renderLoading
};
